from django.db.models import *

# Create your models here.

class Status(Model):
    text = TextField(max_length=300)
    date = DateField(auto_now_add=True)
    time = TimeField(auto_now_add=True)