from django.test import TestCase,Client
from django.urls import resolve
from . import views
from .models import Status
from .forms import TheForm
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase

# Create your tests here.

class TestFormApp(TestCase):

    def test_url_is_available(self):
        response = Client().get("/")
        self.assertEqual(response.status_code,200)

    def test_delete_call_delFunc(self):
        response = resolve("/delete/1")
        self.assertEqual(response.func, views.delete)

    def test_render_landing_page_func(self):
        response = resolve('/')
        self.assertEqual(response.func, views.landing_page_view)

    def test_template_landing_page_true(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, "formApp/landing_page.html")

    def test_contains_text(self):
        response = Client().get("/")
        self.assertContains(response, "You want to add status?")
        self.assertContains(response, "Ayo, Wassup !")
        self.assertContains(response, "Awesome, just click below...")

    def test_contains_modalBtn(self):
        response = Client().get("/")
        self.assertContains(response, "modalBtn")
    
    def test_model_creation(self):
        status = Status(text="abc")
        status.save()
        response = Client().get("/")
        self.assertContains(response, "abc")

    def test_form_creation(self):
        form = TheForm({"text":"xyz"})
        self.assertTrue(form.is_valid())

    def test_delete_redirects(self):
        status = Status(text="abc")
        status.save()
        response = Client().post("/delete/1", {"id":1})
        self.assertEqual(response.status_code, 302)
    
    def test_landing_page_post_creates_model(self):
        response = Client().post("/", {"text":"abc"})
        self.assertEqual(len(Status.objects.all()), 1)


class FormAppFuncTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        super(FormAppFuncTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FormAppFuncTest,self).tearDown()

    def test_functionality(self):
        driver = self.browser
        driver.get(self.live_server_url)

        modal_btn=driver.find_element_by_id("modalBtn")
        modal_btn.click()
        sleep(0.5)
        status = driver.find_element_by_id("id_text")
        sleep(0.5)
        status.send_keys("Coba coba")
        sleep(0.5)
        btn_submit = driver.find_element_by_id("modalSubmit")
        btn_submit.click()
        sleep(0.5)
        self.assertIn("Coba coba",driver.page_source)
        self.assertIn(date.today().strftime("%b. %d, %Y").replace(" 0"," "), driver.page_source)