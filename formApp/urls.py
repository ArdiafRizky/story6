from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = "formApp"

urlpatterns = [
    path('',views.landing_page_view,name="formApp_page"),
    path('delete/<int:id>', views.delete, name="delete"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()