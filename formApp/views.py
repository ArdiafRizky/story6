from django.shortcuts import render,redirect
from formApp.models import Status
from formApp.forms import TheForm
# Create your views here.

def landing_page_view(request):
    statuses = Status.objects.all()
    if(request.method=="POST"):
        form = TheForm(request.POST)
        if(form.is_valid()):
            status = Status(
                text = form.cleaned_data["text"],
            )
            status.save()
            return redirect("formApp:formApp_page")

    form = TheForm()
    return render(request,"formApp/landing_page.html",{"statuses":statuses, "form":form})

def delete(request, id):
    status = Status.objects.get(pk=id)
    status.delete()
    return redirect("formApp:formApp_page")