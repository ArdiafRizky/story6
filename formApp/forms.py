from formApp.models import Status
import django.forms as forms

class TheForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = "__all__"
 