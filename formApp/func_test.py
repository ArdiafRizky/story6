from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase


class FormAppFuncTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(executable_path="./chromedriver")
        super(FormAppFuncTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FormAppFuncTest,self).tearDown()

    def test_functionality(self):
        driver = self.browser
        driver.get(self.live_server_url)

        modal_btn=driver.find_element_by_id("modalBtn")
        modal_btn.click()
        sleep(0.5)
        status = driver.find_element_by_id("id_text")
        sleep(0.5)
        status.send_keys("Coba coba")
        sleep(0.5)
        btn_submit = driver.find_element_by_id("modalSubmit")
        btn_submit.click()
        sleep(0.5)
        self.assertIn("Coba coba",driver.page_source)
        self.assertIn(date.today().strftime("%b. %d, %Y").replace(" 0"," "), driver.page_source)